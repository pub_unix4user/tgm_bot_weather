FROM php:8-cli
RUN apt-get -qy update && apt-get install -qy curl git zip && rm -rf /var/lib/apt/lists/*
COPY --from=composer /usr/bin/composer /usr/bin/composer
RUN composer require longman/telegram-bot
ADD weather.php /
CMD ["php", "/weather.php"]
